SEO Anchors is a module for Drupal 6.x

--INSTALL--
Add to the modules directory and enable. Go to admin/seo_anchors to set defaults
and overide syntax. Check your Input formats for a new filter 'SEO Anchors
Filter' - NB SEO Anchors offers a Filter, not a full Input Format. You may add
this filter to any of your existing input formats.

--HOW IT WORKS--
SEO Anchors works by defining a boundary marker and a set of replacement markers
which will allow control over hyperlink presentation, function and action -
primarily to leave control in the hands of the content editor. If no pattern
like those in the examples are found the defaults on this page will control the
anchor.

SEO Anchors are specified at input using the syntax configuration defined here.
Adding one or more of these patterns will override any defaults established on
the previous settings page. All override patterns live at the end of your
replacement, inside the closing boundary. You can use none, one, or more than
one, as will be required by your anchor and it's hyperlink. But this modules
action do not require users to know the full replacement syntax, only the
boundary markers will be required.

You get a css class attached to each of the supported anchors;
class="seoanchors-internal" -- for the indexable internal link
class="seoanchors-internal-nf" -- for the nofollow attributed internal link
class="seoanchors-external" -- for the direct same window indexable external
link
class="seoanchors-external-nf" -- for the direct same window nofollow external
link
class="seoanchors-external-w" -- for the direct new window indexable external
link
class="seoanchors-external-nf-w" -- for the direct new window nofollow external
link
class="seoanchors-external-e" -- for the exit paged same window indexable
external link
class="seoanchors-external-nf-e" -- for the exit paged same window nofollow
external link
class="seoanchors-external-e-w" -- for the exit paged new window indexable
external link
class="seoanchors-external-nf-e-w" -- for the exit paged new window nofollow
external link

--USAGE--
An SEO Anchor {{www.example.com|link this}} corresponds to the HTML
<a href="http://www.example.com">link this</a>

Beyond that (almost wiki style) you may choose your method for defining a link
that the module will interpret into one of the available anchor types. SEO may
sometimes require nofollow tags for internal links (and therefore stop
spiders/crawlers etc) as well for halting PageRank-Leak from external links. The
times you need that are likely to be one offs, so global, site-wide behaviours
do not suit such problems. It would be preferred to have on demand detailed
overides to the site's linking strategy. SEO Anchors offers that to the filter
system. Also, SEO Anchors can add new windows and exit pages to external links
with similar flexibility.

See the Settings and Syntax pages for more detail.
